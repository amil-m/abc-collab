const colors = require('tailwindcss/colors')

module.exports = {
    purge: {
	enabled: false,
	content: [
	    './src/**/*.html',
	    './src/**/*.js',
	    './src/**/*.svelte']
    },
    darkMode: false, // or 'media' or 'class'
    theme: {
	colors: {
	    transparent: 'transparent',
	    current: 'currentColor',
	    black: colors.black,
	    white: colors.white,
	    gray: colors.trueGray,
	    blue: colors.blue,
	    red: colors.red,
	    green: colors.green,
	    acq: {
		darkest: '#3a5d58',
		DEFAULT: '#A2F5ED'
	    },
	    col: {
		darkest: '#483c0a',
		DEFAULT: '#FFD21A'
	    },
	    dis: {
		darkest: '#22344a',
		DEFAULT: '#7AAEEA'
	    },
	    inv: {
		darkest: '#4e2424',
		DEFAULT: '#F8807F'
	    },
	    pra: {
		darkest: '#3d3248',
		DEFAULT: '#BB98DC'
	    },
	    pro: {
		darkest: '#384621',
		DEFAULT: '#BDEA75'
	    }
	},
	extend: {
	    colors: {
		gray: {
		    150: 'rgb(232, 235, 241)'
		}
	    }
	},
    },
    variants: {
	extend: {},
    },
    plugins: [
	require('@tailwindcss/forms'),
    ],
}
